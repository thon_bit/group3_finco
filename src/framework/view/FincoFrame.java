package framework.view;

import java.awt.*;
import java.util.Vector;

import javax.swing.*;

import framework.controller.AccountManager;
import framework.view.buttons.AddInterestBtn;
import framework.view.tablemodel.AccountListModel;

public class FincoFrame extends javax.swing.JFrame
{
	private static final long serialVersionUID = 1L;
	private final int leftButtonX = 470;
	
    private JTable JTable1;
    private JScrollPane JScrollPane1;
 
    int transactionButtonY = 90;
    int addAccountButtonX = 24;
    
	public FincoFrame()
	{
		setTitle("FinCo - Framework");
		setDefaultCloseOperation(javax.swing.JFrame.DO_NOTHING_ON_CLOSE);
		getContentPane().setLayout(new BorderLayout(0,0));
		setSize(680,350);
		setVisible(false);
		JPanel1.setLayout(null);
		getContentPane().add(BorderLayout.CENTER, JPanel1);
		JPanel1.setBounds(0,0,575,310);
		/*
		/Add five buttons on the pane 
		/for Adding personal account, Adding company account
		/Deposit, Withdraw and Exit from the system
		*/
        JScrollPane1 = new JScrollPane();
        JTable1 = new JTable();        
        
        JPanel1.add(JScrollPane1);
        JScrollPane1.setBounds(12,60,444,240);
        JScrollPane1.getViewport().add(JTable1);
        JTable1.setBounds(0, 0, 420, 0);    
 
		btnAddInterest.setBounds(leftButtonX,20,192,33);
		btnAddInterest.setText("Add interest");
		JPanel1.add(btnAddInterest);
		JButton_Exit.setText("Exit");
		JPanel1.add(JButton_Exit);
		JButton_Exit.setBounds(leftButtonX,255,192,33);

		SymWindow aSymWindow = new SymWindow();
		this.addWindowListener(aSymWindow);
		SymAction lSymAction = new SymAction();
		JButton_Exit.addActionListener(lSymAction);
		
		this.setLocationRelativeTo(null); 
	}
	
	public void setAccountListModel(AccountListModel model){
		JTable1.setModel(model);
	}
	
	public void addAddAccountButtons(framework.view.buttons.PlugButton addAccountButton) {
		JPanel1.add(addAccountButton);
		addAccountButton.setBounds(addAccountButtonX, 20, 192, 33);
    	addAccountButtonX += 216;
    	addAccountButton.setMainWindow(this);
	}

	public void addTransactionButtons(framework.view.buttons.PlugButton b) {
    	JPanel1.add(b);
    	b.setBounds(leftButtonX, transactionButtonY, 192, 33);
    	transactionButtonY += 60;
    	b.setMainWindow(this);
	}
	
	javax.swing.JPanel JPanel1 = new javax.swing.JPanel();
	javax.swing.JButton btnAddInterest= new AddInterestBtn();
	javax.swing.JButton JButton_Exit = new javax.swing.JButton();

	void exitApplication()
	{
		try {
		    	this.setVisible(false);    // hide the Frame
		    	this.dispose();            // free the system resources
		    	System.exit(0);            // close the application
		} catch (Exception e) {
		}
	}

	class SymWindow extends java.awt.event.WindowAdapter
	{
		public void windowClosing(java.awt.event.WindowEvent event)
		{
			Object object = event.getSource();
			if (object == FincoFrame.this)
				BankFrm_windowClosing(event);
		}
	}

	void BankFrm_windowClosing(java.awt.event.WindowEvent event)
	{
		// to do: code goes here.
			 
		BankFrm_windowClosing_Interaction1(event);
	}

	void BankFrm_windowClosing_Interaction1(java.awt.event.WindowEvent event) {
		try {
			this.exitApplication();
		} catch (Exception e) {
		}
	}

	class SymAction implements java.awt.event.ActionListener
	{
		public void actionPerformed(java.awt.event.ActionEvent event)
		{
			Object object = event.getSource();
			if (object == JButton_Exit)
				JButtonExit_actionPerformed(event);
		}
	}
    
    //When the Exit button is pressed this code gets executed
    //this will exit from the system
    void JButtonExit_actionPerformed(java.awt.event.ActionEvent event)
	{
		System.exit(0);
	}

	public Vector<Object> getSelectedRow() {
		 int selection = JTable1.getSelectionModel().getMinSelectionIndex();
		 if (selection >=0){
			 Vector<Object> result = new Vector<>();
			 for(int i=0; i< JTable1.getModel().getColumnCount(); i++){
				 result.addElement(JTable1.getModel().getValueAt(selection, i));
			 }
			 return result;
		 }
		return null;
	}
}
