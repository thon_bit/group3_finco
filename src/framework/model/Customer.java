package framework.model;

import java.util.ArrayList;
import java.util.List;

import javax.mail.MessagingException;

public class Customer implements ICustomer{
	private static final long serialVersionUID = 1L;
	private String name;
	private String street;
	private String city;
	private String state;
	private String email;
	private String zip;
	
	private List<IAccount> accounts;
	
	public Customer(String name, String street, String city, String state,
			String email,String zip) {
		super();
		this.name = name;
		this.street = street;
		this.city = city;
		this.state = state;
		this.email = email;
		this.zip = zip;
		accounts = new ArrayList<>();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	@Override
	public void sendEmail(Email email) {
		try {
			(new framework.utility.SendEmail()).sendEmailMessage(this.getEmail(), email);
			System.out.println("Email sent to " + this.getEmail() + "; " + email.toString());
		} catch (MessagingException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void addAccount(IAccount account) {
		accounts.add(account);
	}
	
}
