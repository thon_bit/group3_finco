package framework.view.buttons;

import java.awt.event.ActionEvent;

import framework.command.GenerateReportCommand;
import framework.controller.AccountManager;
import framework.view.form.DefaultReportView;

public class GenerateReport extends PlugButton {

	private static final long serialVersionUID = 1L;

	public GenerateReport() {
		super("Generate Report");
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		GenerateReportCommand transaction = new GenerateReportCommand();
		AccountManager.INSTANCE.addTransaction(transaction);
		DefaultReportView rv = new DefaultReportView("Report- Bank Accounts");
		rv.setReportString(transaction.getReportString());
		rv.setVisible(true);
	}

}
