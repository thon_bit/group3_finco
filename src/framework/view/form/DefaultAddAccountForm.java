package framework.view.form;

public class DefaultAddAccountForm extends AddAccountForm{	
	private static final long serialVersionUID = 1L;
	
	public DefaultAddAccountForm(){
		setTitle("Add Account");		
		setModal(true);
		getContentPane().setLayout(null);
		setSize(298,339);
		setVisible(false);
		JLabel8.setText("Acc Nr");
		getContentPane().add(JLabel8);
		JLabel8.setForeground(java.awt.Color.black);
		JLabel8.setBounds(12,30,48,24);
		getContentPane().add(JTextField_ACNR);
		JTextField_ACNR.setBounds(120,30,156,20);
		JLabel1.setText("Name");
		getContentPane().add(JLabel1);
		JLabel1.setForeground(java.awt.Color.black);
		JLabel1.setBounds(12,60,48,24);
		JLabel2.setText("Street");
		getContentPane().add(JLabel2);
		JLabel2.setForeground(java.awt.Color.black);
		JLabel2.setBounds(12,90,48,24);
		JLabel3.setText("City");
		getContentPane().add(JLabel3);
		JLabel3.setForeground(java.awt.Color.black);
		JLabel3.setBounds(12,120,48,24);
		JLabel4.setText("State");
		getContentPane().add(JLabel4);
		JLabel4.setForeground(java.awt.Color.black);
		JLabel4.setBounds(12,150,48,24);
		JLabel5.setText("Zip");
		getContentPane().add(JLabel5);
		JLabel5.setForeground(java.awt.Color.black);
		JLabel5.setBounds(12,180,48,24);
		JLabel7.setText("Email");
		getContentPane().add(JLabel7);
		JLabel7.setForeground(java.awt.Color.black);
		JLabel7.setBounds(12,210,48,24);
		getContentPane().add(JTextField_NAME);
		JTextField_NAME.setBounds(120,60,156,20);
		getContentPane().add(JTextField_CT);
		JTextField_CT.setBounds(120,120,156,20);
		getContentPane().add(JTextField_ST);
		JTextField_ST.setBounds(120,150,156,20);
		getContentPane().add(JTextField_STR);
		JTextField_STR.setBounds(120,90,156,20);
		getContentPane().add(JTextField_ZIP);
		JTextField_ZIP.setBounds(120,180,156,20);
		getContentPane().add(JTextField_EM);
		JTextField_EM.setBounds(120,210,156,20);
		
		
		JButton_OK.setText("OK");
		JButton_OK.setActionCommand("OK");
		getContentPane().add(JButton_OK);
		JButton_OK.setBounds(48,264,84,24);
		JButton_Cancel.setText("Cancel");
		JButton_Cancel.setActionCommand("Cancel");
		getContentPane().add(JButton_Cancel);
		JButton_Cancel.setBounds(156,264,84,24);
		JButton_OK.addActionListener(this);
		JButton_Cancel.addActionListener(this);
		
		this.setLocationRelativeTo(null);
	}
	
	javax.swing.JLabel JLabel1 = new javax.swing.JLabel();
	javax.swing.JLabel JLabel2 = new javax.swing.JLabel();
	javax.swing.JLabel JLabel3 = new javax.swing.JLabel();
	javax.swing.JLabel JLabel4 = new javax.swing.JLabel();
	javax.swing.JLabel JLabel5 = new javax.swing.JLabel();
	javax.swing.JLabel JLabel7 = new javax.swing.JLabel();
	javax.swing.JLabel JLabel8 = new javax.swing.JLabel();
	
	javax.swing.JTextField JTextField_NAME = new javax.swing.JTextField();
	javax.swing.JTextField JTextField_CT = new javax.swing.JTextField();
	javax.swing.JTextField JTextField_ST = new javax.swing.JTextField();
	javax.swing.JTextField JTextField_STR = new javax.swing.JTextField();
	javax.swing.JTextField JTextField_ZIP = new javax.swing.JTextField();
	javax.swing.JTextField JTextField_EM = new javax.swing.JTextField();	
	javax.swing.JTextField JTextField_ACNR = new javax.swing.JTextField();
	

	public String getAccountType(){
		return "";
	}
	
	public String getAccountIdentifier(){
		return JTextField_ACNR.getText();
	}

	public String getAccountName() {
		return JTextField_NAME.getText();
	}

	public String getStreet(){
		return JTextField_STR.getText();
	}
	public String getState(){
		return JTextField_ST.getText();
	}
	public String getCity(){
		return JTextField_CT.getText();
	}
	public String getZip(){
		return JTextField_ZIP.getText();
	}
	public String getEmail(){
		return JTextField_EM.getText();
	}

}