package banking.view.tablemodel;

import java.util.Arrays;
import java.util.List;
import java.util.Vector;
import framework.model.IAccount;
import framework.view.tablemodel.AccountListModel;

public class BankAccountListModel extends AccountListModel{

	private static final long serialVersionUID = 1L;

	@Override
	public List<String> getColumnList() {
		return Arrays.asList("AccountNr", "Name", "City", "P/C", "Ch/S", "Amount");
	}

	@Override
	public void update(IAccount account) {
		String accountNumber = account.getIdentifier();		
		Vector<String> newRow = new Vector<>();
		newRow.addElement(account.getIdentifier());
		framework.model.ICustomer customer = account.getCustomer();
		newRow.addElement(customer.getName());
		newRow.addElement(customer.getCity());
		newRow.addElement(customer instanceof banking.model.Company? "C": "P");
		newRow.addElement(account.getAccountType().equals("checking")? "Ch": "S");
		newRow.addElement(String.valueOf(account.getBalance()));
		updateModel(accountNumber, newRow);
	}

	@Override
	public String getIdColumnName() {
		return "AccountNr";
	}
}
