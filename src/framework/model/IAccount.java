package framework.model;

import java.io.Serializable;
import java.util.Iterator;

public interface IAccount extends Serializable{
	void setCustomer(ICustomer customer);
	ICustomer getCustomer();
	String getAccountType();
	double getBalance();
	double getInterestRate();
	void addEntry(IEntry entry);
	String getIdentifier();
	Iterator<IEntry> getEntriesIterator();
}
