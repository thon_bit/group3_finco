package framework.utility;

import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;

public class XMLSerializeUtility {
	public static <T>void SerializeXML(T object,String filename)throws Exception{
		XMLEncoder encoder=new XMLEncoder(new BufferedOutputStream(new FileOutputStream(filename)));
		encoder.writeObject(object);
		encoder.close();
	}
	@SuppressWarnings({ "unchecked", "resource" })
	public static <T>T DeSerializeXML(String filename)throws Exception{
		XMLDecoder decoder=null;
		decoder=new XMLDecoder(new BufferedInputStream(new FileInputStream(filename)));
		return (T)decoder.readObject();
	}
	@SuppressWarnings("unchecked")
	public static <T>T newInstance(String className) throws Exception{
		Class<?> cl = Class.forName(className);
		return (T)cl.newInstance();
	}
}
