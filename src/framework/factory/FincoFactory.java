package framework.factory;

import framework.model.IAccount;
import framework.model.ICustomer;
import framework.view.form.AddAccountForm;

/*
 * Abstract Factory Pattern
 */
public interface FincoFactory {
	public IAccount createAccount(AddAccountForm formData);
	public ICustomer createCustomer(AddAccountForm formData);
}
