package framework.dataaccesssubsystem;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import framework.model.IAccount;

public final class DefaultDataAccess implements DataAccessFacade {
	List<IAccount> accounts;
	private static final String STORAGE_FILE = System.getProperty("user.dir")
			+ "\\finco_storage\\FinCo_default_db.txt";
	
	@SuppressWarnings("unchecked")
	public DefaultDataAccess(){
		try{
			accounts = (List<IAccount>) readData();
		}catch(Exception ex){
			ex.printStackTrace();
		}
		if(accounts == null){
			accounts = new ArrayList<IAccount>();
		}
	}
	@Override
	public void save(IAccount account) {
		removeByIdentifier(account);
		accounts.add(account);
		writeData(accounts);
		
	}
	private void removeByIdentifier(IAccount account) {
		for(IAccount a: accounts){
			if(a.getIdentifier().equals(account.getIdentifier())){
				accounts.remove(a);
				break;
			}
		}
	}

	@Override
	public void remove(IAccount account) {
		removeByIdentifier(account);
		writeData(accounts);
	}

	@Override
	public List<IAccount> getAll() {
		return accounts;
	}

	@Override
	public void save(List<IAccount> accounts) {
		this.accounts = accounts;
		writeData(accounts);
	}
	
	private void writeData(Object data){

		ObjectOutputStream out = null;
		try {
			Path path = FileSystems.getDefault().getPath(STORAGE_FILE);
			out = new ObjectOutputStream(Files.newOutputStream(path));
			out.writeObject(data);
		}catch(IOException e){
			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		}
		finally {
			if (out != null) {
				try {
					out.close();
				} catch (Exception e) {
				}
			}
		}

	}
	
	public Object readData() throws IOException, ClassNotFoundException {
		ObjectInputStream in = null;
		Object data = null;
		try {			
			Path path = FileSystems.getDefault().getPath(STORAGE_FILE);
			if(Files.exists(path, LinkOption.NOFOLLOW_LINKS)){
				in = new ObjectInputStream(Files.newInputStream(path));
				data = in.readObject();
			}			
		}
		finally {
			if (in != null) {
				try {
					in.close();
				} catch (Exception e) {
				}
			}
		}
		return data;
	}

}
