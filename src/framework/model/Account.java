package framework.model;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public abstract class Account implements IAccount{
	private static final long serialVersionUID = 1L;
	private ICustomer customer;
	private double balance;
	private List<IEntry> entries;
	
	public Account(){
		entries = new ArrayList<IEntry>();
	}
	@Override
	public final ICustomer getCustomer() {
		return customer;
	}

	@Override
	public final double getBalance() {
		return balance;
	}

	@Override
	public abstract double getInterestRate();

	@Override
	public final void addEntry(IEntry entry) {
		if(entries == null){
			entries = new ArrayList<IEntry>();
		}
		balance += entry.getAmount();
		entries.add(entry);
	}

	@Override
	public final Iterator<IEntry> getEntriesIterator() {
		return entries.iterator();
	}
	
	@Override
	public final void setCustomer(ICustomer customer) {
		this.customer = customer;
	}

}
