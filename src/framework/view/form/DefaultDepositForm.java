package framework.view.form;

import framework.command.DepositCommand;
import framework.controller.AccountManager;

public class DefaultDepositForm extends TransactionForm{

	private static final long serialVersionUID = 1L;
	public DefaultDepositForm(String accountIdentifier) {
		super(accountIdentifier, "Deposit");
		// TODO Auto-generated constructor stub
	}

	@Override
	public void process(String accountIdentifier, double inputValue) {
		DepositCommand tx = new DepositCommand(accountIdentifier, inputValue);
		AccountManager.INSTANCE.addTransaction(tx);
	}

}
