package banking.factory;

import banking.model.CheckingAccount;
import banking.model.Company;
import banking.model.Person;
import banking.model.SavingAccount;
import banking.view.forms.AddCompanyAccountForm;
import banking.view.forms.AddPersonalAcccountForm;
import framework.factory.FincoFactory;
import framework.model.IAccount;
import framework.model.ICustomer;
import framework.view.form.AddAccountForm;

public class BankAccountFactory implements FincoFactory{

	@Override
	public IAccount createAccount(AddAccountForm formData) {
		IAccount acc =null;
		switch(formData.getAccountType()){
		case "saving":
			acc = new SavingAccount(formData.getAccountIdentifier());
			break;
		case "checking":
			acc = new CheckingAccount(formData.getAccountIdentifier()); 
			break;
		}
		return acc;
	}

	@Override
	public ICustomer createCustomer(AddAccountForm formData) {
		ICustomer customer = null;
		if(formData instanceof AddPersonalAcccountForm){
			AddPersonalAcccountForm form = (AddPersonalAcccountForm) formData;
			customer = new Person(form.getAccountName(), form.getStreet(), form.getCity(), form.getState(), form.getEmail(), form.getBirthDate(),form.getZip());
		}else{
			AddCompanyAccountForm form = (AddCompanyAccountForm) formData;
			customer = new Company(form.getAccountName(), form.getStreet(), form.getCity(), form.getState(), form.getEmail(), form.getZip(), form.getNoOfEmp());
		}
		return customer;
	}

}
