package framework.model;

import java.io.Serializable;
import java.time.LocalDate;

public interface IEntry extends Serializable{
	double getAmount();
	String getDescription();
	LocalDate getDate();
}
