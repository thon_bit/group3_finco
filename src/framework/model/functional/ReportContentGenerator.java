package framework.model.functional;

import java.util.Iterator;

import framework.model.IAccount;
import framework.model.IEntry;

public class ReportContentGenerator implements Functor {
	
	StringBuilder stringBuilder = new StringBuilder();

	@Override
	public void execute(IAccount acc) {
		stringBuilder.append("\n---------*****************************---------");
		stringBuilder.append("\nAccount Id: " + acc.getIdentifier());
		stringBuilder.append("\nAccount Name: " + acc.getCustomer().getName());
		stringBuilder.append("\nCity: " + acc.getCustomer().getCity());
		stringBuilder.append("\nBalance: " + acc.getBalance());
		stringBuilder.append("\n--------------------");
		stringBuilder.append("\nTransaction History");
		stringBuilder.append("\n--------------------");
		Iterator<IEntry> it = acc.getEntriesIterator();
		while(it.hasNext()){
			IEntry entry = it.next();
			stringBuilder.append("\n" + entry.toString());
		}		
		stringBuilder.append("\n---------*****************************---------");
		stringBuilder.append("\n");
		stringBuilder.append("\n");
	}
	
	public String getReportString(){
		return stringBuilder.toString();
	}

}
