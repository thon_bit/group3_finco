package framework.command;

import java.time.LocalDate;

import framework.model.Entry;
import framework.model.functional.AccountIdentifierMatcher;
import framework.model.functional.TransactionFunctor;

public class DepositCommand extends Transaction {
	
	private String accountNumber;
	private Double value;
	
	public DepositCommand(String accountNumber, Double value){
		this.accountNumber = accountNumber;
		this.value = value;
	}

	@Override
	public void execute() {
		Entry entry = new Entry("DEPOSIT", LocalDate.now(), value);
		AccountIdentifierMatcher p = new AccountIdentifierMatcher(accountNumber);
		TransactionFunctor f = new TransactionFunctor(entry);
		getReceiver().doTransaction(p, f);
	}

}
