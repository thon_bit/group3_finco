package framework.command;

import java.util.Iterator;

import framework.model.AccountList;
import framework.model.IAccount;
import framework.model.functional.ReportContentGenerator;

public class GenerateReportCommand extends Transaction{

	ReportContentGenerator f = new ReportContentGenerator();
	
	@Override
	public void execute() {
		Iterator<IAccount> it = AccountList.INSTANCE.getIterator();
		while(it.hasNext()){
			f.execute(it.next());
		}
	}

	public String getReportString() {
		return f.getReportString();
	}

}
