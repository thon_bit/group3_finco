package framework.view.buttons;

import java.awt.event.ActionEvent;

import javax.swing.JOptionPane;

import framework.command.AddInterest;
import framework.controller.AccountManager;

public class AddInterestBtn extends PlugButton {
	private static final long serialVersionUID = 1L;

	public AddInterestBtn() {
		super("Add Interest");
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		  JOptionPane.showMessageDialog(this, "Add interest to all accounts","Add interest to all accounts",JOptionPane.WARNING_MESSAGE);
		  framework.command.Transaction trns = new AddInterest();
		  AccountManager.INSTANCE.addTransaction(trns);
	}

}
