package framework.utility;

import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import framework.configuration.MailConfiguration;
import framework.model.Email;

public class SendEmail {
	private class SMTPAuthenticator extends Authenticator {
		public PasswordAuthentication getPasswordAuthentication() {
			return new PasswordAuthentication(
					MailConfiguration.INSTANCE.getEmailFrom(),
					MailConfiguration.INSTANCE.getPassword());
		}
	}

	public void sendEmailMessage(String to, Email email) throws MessagingException {

		// Get system properties
		Properties props = System.getProperties();
		props = new Properties();
		props.put("mail.smtp.user", MailConfiguration.INSTANCE.getEmailFrom());
		props.put("mail.smtp.host", MailConfiguration.INSTANCE.getSmtpHost());
		props.put("mail.smtp.port", MailConfiguration.INSTANCE.getSmtpPort());
		props.put("mail.smtp.starttls.enable", MailConfiguration.INSTANCE.getStarttlsEnabled());
		props.put("mail.smtp.debug", MailConfiguration.INSTANCE.getSmtpDebug());
		props.put("mail.smtp.auth", MailConfiguration.INSTANCE.getSmtpAuth());
		props.put("mail.smtp.socketFactory.port", MailConfiguration.INSTANCE.getSocketFactoryPort());
		props.put("mail.smtp.socketFactory.class",
				MailConfiguration.INSTANCE.getSocketFactoryClass());
		props.put("mail.smtp.socketFactory.fallback", MailConfiguration.INSTANCE.getSocketFactoryFallback());

		SMTPAuthenticator auth = new SMTPAuthenticator();
		Session session = Session.getInstance(props, auth);
		session.setDebug(false);

		MimeMessage msg = new MimeMessage(session);
		msg.setText(email.getMessage());
		msg.setSubject(email.getSubject());
		msg.setFrom(new InternetAddress(MailConfiguration.INSTANCE.getEmailFrom()));
		msg.addRecipient(Message.RecipientType.TO, new InternetAddress(
				to));

		Transport transport = session.getTransport("smtps");
		transport.connect(MailConfiguration.INSTANCE.getSmtpHost(), 465, "username", "password");
		transport.sendMessage(msg, msg.getAllRecipients());
		transport.close();

	}

}
