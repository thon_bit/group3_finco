package banking.view.buttons;

import java.awt.event.ActionEvent;

import banking.view.forms.AddCompanyAccountForm;

public class AddCompanyAccountBtn  extends framework.view.buttons.PlugButton{

	private static final long serialVersionUID = 1L;

	public AddCompanyAccountBtn() {
		super("Add Company Account");
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		AddCompanyAccountForm pac = new AddCompanyAccountForm();
		pac.setBounds(450, 20, 300, 330);
		pac.setVisible(true);
	}


}
