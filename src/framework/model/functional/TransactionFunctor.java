package framework.model.functional;

import framework.model.Email;
import framework.model.IAccount;
import framework.model.IEntry;

public class TransactionFunctor implements Functor {
	
	IEntry entry = null;
	
	public TransactionFunctor(IEntry e) {
		this.entry = e;
	}

	@Override
	public void execute(IAccount acc) {
		acc.addEntry(entry);
		if(acc.getCustomer() instanceof banking.model.Person && (Math.abs(entry.getAmount()) > 500 || acc.getBalance() < 0)){
			String subject = "Notification of transaction: " + entry.getDescription();
			String message = "Dear Customer, a transaction has been performed in your account.";
			Email email = new Email(subject, message);
			acc.getCustomer().sendEmail(email);
		}
		if(acc.getCustomer() instanceof banking.model.Company){
			String subject = "Notification of transaction: " + entry.getDescription();
			String message = "Dear Customer, a transaction has been performed in your account.";
			Email email = new Email(subject, message);
			acc.getCustomer().sendEmail(email);
		}
	}

}
