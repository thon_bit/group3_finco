package framework.view.buttons;

import java.awt.event.ActionEvent;
import java.util.Vector;

import javax.swing.JOptionPane;

import framework.view.form.DefaultDepositForm;

public class DepositButton extends PlugButton {

	private static final long serialVersionUID = 1L;

	public DepositButton() {
		super("Deposit");
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		Vector<Object> selected = getSelectedRow();
		if(selected != null){
			String accountNumber = (String) selected.get(0);
			(new DefaultDepositForm(accountNumber)).setVisible(true);
		}else{
			JOptionPane.showConfirmDialog(this, "Please choose an account first.");
		}
	}

}
