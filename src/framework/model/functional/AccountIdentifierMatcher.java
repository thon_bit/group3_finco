package framework.model.functional;

import framework.model.IAccount;

public class AccountIdentifierMatcher implements Predicate{
	
	String accountNumber ;
	
	public AccountIdentifierMatcher(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	@Override
	public boolean test(IAccount acc) {
		return acc.getIdentifier().equals(accountNumber);
	}

}
