package framework.command;

import framework.model.AccountList;

public abstract class Transaction {
	
	public abstract void execute();
	
	public AccountList getReceiver(){
		return AccountList.INSTANCE;
	}
}
