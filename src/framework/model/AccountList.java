package framework.model;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Observer;

import framework.dataaccesssubsystem.DataAccessFacade;
import framework.model.functional.*;

public enum AccountList {
	INSTANCE;

	private List<IAccount> accountList;
	private List<Observer> observers;
	private DataAccessFacade repository;

	private AccountList() {		
		this.observers = new ArrayList<>();
		this.accountList = new ArrayList<>();
	}
	
	public void setRepository(DataAccessFacade daFacade){
		this.repository = daFacade;
		this.accountList = repository.getAll();
		if(this.accountList == null){
			this.accountList = new ArrayList<>();
		}
		notify(this);
	}

	public void addAccount(IAccount acc) {
		accountList.add(acc);
		repository.save(acc);
		notify(this);
	}

	public void addObserver(Observer observer) {
		this.observers.add(observer);
		notify(this);
	}

	public void removeAccount(IAccount acc) {
		accountList.remove(acc);
		repository.remove(acc);
		notify(this);
	}

	public Iterator<IAccount> getIterator() {
		return accountList.iterator();
	}

	public void doTransaction(Functor func) {
		Iterator<IAccount> iterator = accountList.iterator();
		while (iterator.hasNext()) {
			IAccount acc = iterator.next();
			func.execute(acc);
		}
		repository.save(accountList);
		notify(this);
	}

	public void doTransaction(Predicate pre, Functor func) {
		Iterator<IAccount> iterator = accountList.iterator();
		while (iterator.hasNext()) {
			IAccount acc = iterator.next();
			if (pre.test(acc)) {
				func.execute(acc);
			}
		}		
		repository.save(accountList);
		notify(this);
	}

	public void notify(Object arg) {
		for (Observer observer : observers) {
			observer.update(null, arg);
		}
	}
}
