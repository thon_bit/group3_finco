package framework.view.form;

import framework.command.WithdrawCommand;
import framework.controller.AccountManager;

public class DefaultWithdrawForm extends TransactionForm {
	
	private static final long serialVersionUID = 1L;

	public DefaultWithdrawForm(String accountIdentifier) {
		super(accountIdentifier, "Withdraw");
	}

	@Override
	public void process(String accountIdentifier, double inputValue) {
		WithdrawCommand tx = new WithdrawCommand(accountIdentifier, inputValue);
		AccountManager.INSTANCE.addTransaction(tx);
	}

}
