package banking.view.forms;

import framework.command.DepositCommand;
import framework.controller.AccountManager;
import framework.view.form.TransactionForm;

public class DepositForm extends TransactionForm{

	private static final long serialVersionUID = 1L;

	public DepositForm(String accountIdentifier) {
		super(accountIdentifier, "Deposit");
	}

	@Override
	public void process(String accountIdentifier, double inputValue) {
		DepositCommand tx = new DepositCommand(accountIdentifier, inputValue);
		AccountManager.INSTANCE.addTransaction(tx);
	}

}
