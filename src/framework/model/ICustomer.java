package framework.model;

import java.io.Serializable;

public interface ICustomer extends Serializable{
	void addAccount(IAccount account);
	void sendEmail(Email email);
	String getName();
	String getCity();
}
