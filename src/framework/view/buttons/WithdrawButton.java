package framework.view.buttons;

import java.awt.event.ActionEvent;
import java.util.Vector;

import javax.swing.JOptionPane;

public class WithdrawButton extends PlugButton{

	private static final long serialVersionUID = 1L;

	public WithdrawButton() {
		super("Withdraw");
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Vector<Object> selected = getSelectedRow();
		if(selected != null){
			String accountNumber = (String) selected.get(0);
			(new framework.view.form.DefaultWithdrawForm(accountNumber)).setVisible(true);
		}else{
			JOptionPane.showConfirmDialog(this, "Please choose an account first.");
		}
	}

}
