package framework.model;

import java.time.LocalDate;

public class Entry implements IEntry{
	private static final long serialVersionUID = 1L;
	private String type;
	private LocalDate date;
	private double amount;
	
	
	public Entry(String type, LocalDate date, double amount) {
		super();
		this.type = type;
		this.date = date;
		this.amount = amount;
	}

	@Override
	public double getAmount() {
		// TODO Auto-generated method stub
		return amount;
	}

	@Override
	public String getDescription() {
		// TODO Auto-generated method stub
		return type;
	}
	
	@Override
	public LocalDate getDate(){
		return date;
	}
	
	@Override
	public String toString() {
		return "{date = " + date + ", amount= " + amount + ", " + type + "}";
	}

}
