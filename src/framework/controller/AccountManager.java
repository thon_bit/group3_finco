package framework.controller;

import framework.factory.FincoFactory;

public enum AccountManager {
	INSTANCE;
	
	FincoFactory accountFactory;
	
	public void setAccountFactory(FincoFactory accountFactory) {
		this.accountFactory = accountFactory;
	}
	
	public FincoFactory getAccountFactory(){
		return this.accountFactory;
	}
	
	public void addTransaction(framework.command.Transaction transaction){
		transaction.execute();
	}
}
