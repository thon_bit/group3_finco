package framework.command;

import framework.factory.FincoFactory;
import framework.model.IAccount;
import framework.model.ICustomer;
import framework.view.form.AddAccountForm;

public class CreateAccount extends Transaction {
	
	AddAccountForm formData;
	FincoFactory factory;
	
	public CreateAccount(AddAccountForm formData, FincoFactory factory){
		this.formData = formData;
		this.factory = factory;
	}

	@Override
	public void execute() {
		IAccount account = factory.createAccount(formData);
		ICustomer customer = factory.createCustomer(formData);
		account.setCustomer(customer);
		customer.addAccount(account);	
		getReceiver().addAccount(account);
	}

}
