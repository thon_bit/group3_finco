package framework.factory;

import framework.model.Customer;
import framework.model.IAccount;
import framework.model.ICustomer;
import framework.model.defaults.DefaultAccount;
import framework.view.form.AddAccountForm;

public class DefaultFincoFactory implements FincoFactory {

	@Override
	public IAccount createAccount(AddAccountForm formData) {
		return new DefaultAccount(formData.getAccountIdentifier());
	}

	@Override
	public ICustomer createCustomer(AddAccountForm form) {
		Customer c = new Customer(form.getAccountName(), form.getStreet(), form.getCity(), form.getState(), form.getEmail(), form.getZip());
		return c;
	}

}
