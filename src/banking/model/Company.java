package banking.model;

import framework.model.Customer;
import framework.model.ICompany;

public class Company extends Customer implements ICompany{
	private static final long serialVersionUID = 1L;
	private String noofemp;
	
	public Company(String name, String street, String city, String state,
			String email, String zip,String noofemp) {
		super(name, street, city, state, email, zip);
		// TODO Auto-generated constructor stub
		this.noofemp = noofemp;
	}
	
	public void setNoOfEmp(String noofemp){
		this.noofemp = noofemp;
	}

	@Override
	public int getNumberOfEmployees() {
		return Integer.valueOf(noofemp);
	}

}
