package framework.view.tablemodel;

import java.util.Arrays;
import java.util.List;
import java.util.Vector;

import framework.model.IAccount;

public class DefaultAccountListModel extends AccountListModel{

	private static final long serialVersionUID = 1L;

	@Override
	public List<String> getColumnList() {
		return Arrays.asList("AccountNr", "Name", "City", "Amount");
	}

	@Override
	public String getIdColumnName() {
		return "AccountNr";
	}

	@Override
	public void update(IAccount account) {
		String accountNumber = account.getIdentifier();		
		Vector<String> newRow = new Vector<>();
		newRow.addElement(account.getIdentifier());
		framework.model.ICustomer customer = account.getCustomer();
		newRow.addElement(customer.getName());
		newRow.addElement(customer.getCity());
		newRow.addElement(String.valueOf(account.getBalance()));
		updateModel(accountNumber, newRow);
	}

}
