package banking.view.forms;

import framework.command.WithdrawCommand;
import framework.controller.AccountManager;
import framework.view.form.TransactionForm;

public class WithdrawForm extends TransactionForm{

	private static final long serialVersionUID = 1L;

	public WithdrawForm(String accountIdentifier) {
		super(accountIdentifier, "Deposit");
	}

	@Override
	public void process(String accountIdentifier, double inputValue) {
		WithdrawCommand tx = new WithdrawCommand(accountIdentifier, inputValue);
		AccountManager.INSTANCE.addTransaction(tx);
	}
}
