package framework.view.tablemodel;

import java.util.Iterator;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.Vector;

import javax.swing.table.DefaultTableModel;

import framework.model.AccountList;
import framework.model.IAccount;

public abstract class AccountListModel extends DefaultTableModel implements Observer{

	private static final long serialVersionUID = 1L;

	public AccountListModel(){
		List<String> columns = getColumnList();
		for(String column: columns){
			this.addColumn(column);
		}
	}
	
	@Override
	public final void update(Observable arg0, Object arg1) {
		if(arg1 instanceof AccountList)
			update(AccountList.INSTANCE);
		if(arg1 instanceof IAccount){
			update((IAccount)arg1);
		}
	}

	public abstract List<String> getColumnList();
	
	public abstract String getIdColumnName();
	
	public abstract void update(IAccount account);

	public void update(AccountList accountList){
		clear();
		Iterator<IAccount> it = accountList.getIterator();
		while(it.hasNext()){
			IAccount account = it.next();
			update(account);
		}
	}
	
	public void clear() {
		int rowcount = this.getRowCount();
		for(int i = rowcount - 1; i >= 0; i--){
			this.removeRow(i);
		}
	}
	
	public int findRowByAccountIdentifier(String idValue){
		int j = this.findColumn(getIdColumnName());
		for(int i = 0; i< this.getRowCount(); i++){
			if(idValue.equals(this.getValueAt(i, j))){
				return i;
			}
		}
		return -1;
		
	}
	
	public void updateModel(String identifier, Vector<String> newRow) {
		int index = findRowByAccountIdentifier(identifier);
		if(index >= 0){
			int col = 0;
			for(String value: newRow){
				this.setValueAt(value, index, col);
				col ++;
			}
		}else{
			this.addRow(newRow);
		}
	}

}
