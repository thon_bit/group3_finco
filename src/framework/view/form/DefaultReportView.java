package framework.view.form;

public class DefaultReportView extends javax.swing.JDialog{

	private static final long serialVersionUID = 1L;
	
	public DefaultReportView(String title){
		this.setTitle(title);
		getContentPane().setLayout(null);
		setSize(820,540);
		setVisible(false);
		getContentPane().add(JScrollPane1);
		JScrollPane1.setBounds(24,24,750,450);
		JScrollPane1.getViewport().add(JTextField1);
		JTextField1.setBounds(0,0,700,400);
		
		this.setLocationRelativeTo(null); 
	}
	
	javax.swing.JScrollPane JScrollPane1 = new javax.swing.JScrollPane();
	javax.swing.JTextArea  JTextField1 = new javax.swing.JTextArea ();
	
	public void setReportString(String reportText){
		JTextField1.setText(reportText);
	}

}
