package banking.model;

import framework.model.Account;

public class CheckingAccount extends Account{
	private static final long serialVersionUID = 1L;
	private String accountNumber;
	
	public CheckingAccount(String accountNumber){
		super();
		this.accountNumber = accountNumber;
	}
	
	@Override
	public String getAccountType() {
		return "checking";
	}
	
	@Override
	public String getIdentifier() {
		return accountNumber;
	}

	@Override
	public double getInterestRate() {
		return 0.05;
	}
}
