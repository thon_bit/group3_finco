package framework.configuration;

public enum MailConfiguration {
	INSTANCE;
	private String emailFrom = "finco.group3@gmail.com";
	private String password = "FincoGrp3";
	private String smtpHost = "smtp.gmail.com";
	private String smtpPort = "587";
	private String starttlsEnabled = "true";
	private String smtpDebug = "true";
	private String smtpAuth = "true";
	private String socketFactoryPort = "587";
	private String socketFactoryClass = "javax.net.ssl.SSLSocketFactory";
	private String socketFactoryFallback = "false";
	public String getEmailFrom() {
		return emailFrom;
	}
	public void setEmailFrom(String emailFrom) {
		this.emailFrom = emailFrom;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getSmtpHost() {
		return smtpHost;
	}
	public void setSmtpHost(String smtpHost) {
		this.smtpHost = smtpHost;
	}
	public String getSmtpPort() {
		return smtpPort;
	}
	public void setSmtpPort(String smtpPort) {
		this.smtpPort = smtpPort;
	}
	public String getStarttlsEnabled() {
		return starttlsEnabled;
	}
	public void setStarttlsEnabled(String starttlsEnabled) {
		this.starttlsEnabled = starttlsEnabled;
	}
	public String getSmtpDebug() {
		return smtpDebug;
	}
	public void setSmtpDebug(String smtpDebug) {
		this.smtpDebug = smtpDebug;
	}
	public String getSmtpAuth() {
		return smtpAuth;
	}
	public void setSmtpAuth(String smtpAuth) {
		this.smtpAuth = smtpAuth;
	}
	public String getSocketFactoryPort() {
		return socketFactoryPort;
	}
	public void setSocketFactoryPort(String socketFactoryPort) {
		this.socketFactoryPort = socketFactoryPort;
	}
	public String getSocketFactoryClass() {
		return socketFactoryClass;
	}
	public void setSocketFactoryClass(String socketFactoryClass) {
		this.socketFactoryClass = socketFactoryClass;
	}
	public String getSocketFactoryFallback() {
		return socketFactoryFallback;
	}
	public void setSocketFactoryFallback(String socketFactoryFallback) {
		this.socketFactoryFallback = socketFactoryFallback;
	}
	
}
