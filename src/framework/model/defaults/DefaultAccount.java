package framework.model.defaults;

import framework.model.Account;

public class DefaultAccount extends Account {

	private static final long serialVersionUID = 1L;
	private String identifier;
	
	public DefaultAccount(String identifier) {
		this.identifier = identifier;
	}

	@Override
	public String getAccountType() {
		return "";
	}

	@Override
	public String getIdentifier() {
		return identifier;
	}

	@Override
	public double getInterestRate() {
		// TODO Auto-generated method stub
		return 0.02;
	}

}
