package framework.model;

public interface ICompany extends ICustomer {
	int getNumberOfEmployees();
}
