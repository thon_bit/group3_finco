package framework.model.functional;

import framework.model.IAccount;

@FunctionalInterface
public interface Functor {
	void execute(IAccount acc);
}
