package framework.command;

import framework.model.functional.AddInterestFunctor;

public class AddInterest extends Transaction {

	@Override
	public void execute() {
		AddInterestFunctor func = new AddInterestFunctor();
		getReceiver().doTransaction(func);
	}

}
