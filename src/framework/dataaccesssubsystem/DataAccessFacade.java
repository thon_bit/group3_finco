package framework.dataaccesssubsystem;

import java.util.List;

import framework.model.IAccount;

public interface DataAccessFacade {
	void save(IAccount account);
	void remove(IAccount account);
	List<IAccount> getAll();
	void save(List<IAccount> accounts);
}
