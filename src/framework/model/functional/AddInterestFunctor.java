package framework.model.functional;

import java.time.LocalDate;

import framework.model.Entry;
import framework.model.IAccount;
import framework.model.IEntry;

public class AddInterestFunctor implements Functor{

	@Override
	public void execute(IAccount account) {
		Double balance = account.getBalance();
		Double rate = account.getInterestRate();
		Double interest = calculateInterest(balance, rate);
		LocalDate date = LocalDate.now();
		IEntry e = new Entry("INTEREST", date, interest);
		account.addEntry(e);
	}

	private Double calculateInterest(Double balance, Double rate) {
		Double interest = balance * rate;
		return interest;
	}

}
