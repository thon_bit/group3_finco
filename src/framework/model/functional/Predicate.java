package framework.model.functional;

import framework.model.IAccount;

public interface Predicate {
	boolean test(IAccount acc);
}
