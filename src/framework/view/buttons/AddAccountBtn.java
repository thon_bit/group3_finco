package framework.view.buttons;

import java.awt.event.ActionEvent;

import framework.view.form.AddAccountForm;
import framework.view.form.DefaultAddAccountForm;

public class AddAccountBtn extends PlugButton{

	private static final long serialVersionUID = 1L;

	public AddAccountBtn() {
		super("Add Account");
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		AddAccountForm form = new DefaultAddAccountForm();
		form.setVisible(true);
	}

}
