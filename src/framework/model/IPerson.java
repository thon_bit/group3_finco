package framework.model;

public interface IPerson extends ICustomer {
	String getBirthdate();
}
