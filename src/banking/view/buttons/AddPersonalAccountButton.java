package banking.view.buttons;

import java.awt.event.ActionEvent;

import banking.view.forms.AddPersonalAcccountForm;
import framework.view.buttons.PlugButton;

public class AddPersonalAccountButton extends PlugButton{

	private static final long serialVersionUID = 1L;

	public AddPersonalAccountButton() {
		super("Add Personal Account");
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		AddPersonalAcccountForm pac = new AddPersonalAcccountForm();
		pac.setBounds(450, 20, 300, 330);
		pac.setVisible(true);
	}

}
