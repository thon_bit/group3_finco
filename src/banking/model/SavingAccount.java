package banking.model;

import framework.model.Account;

public class SavingAccount extends Account{
	static final long serialVersionUID = 1L;
	private String accountNumber;
	
	public SavingAccount(String accountNumber){
		super();
		this.accountNumber = accountNumber;
	}
	
	@Override
	public String getAccountType() {
		return "saving";
	}

	@Override
	public String getIdentifier() {
		return accountNumber;
	}

	@Override
	public double getInterestRate() {
		// TODO Auto-generated method stub
		return 0.07;
	}
	
}
