package framework;

import javax.swing.UIManager;

import framework.configuration.Configuration;
import framework.configuration.MailConfiguration;
import framework.controller.AccountManager;
import framework.dataaccesssubsystem.DataAccessFacade;
import framework.dataaccesssubsystem.DefaultDataAccess;
import framework.factory.FincoFactory;
import framework.factory.DefaultFincoFactory;
import framework.model.AccountList;
import framework.utility.XMLSerializeUtility;
import framework.view.FincoFrame;
import framework.view.buttons.DepositButton;
import framework.view.buttons.GenerateReport;
import framework.view.buttons.PlugButton;
import framework.view.buttons.WithdrawButton;
import framework.view.tablemodel.AccountListModel;
import framework.view.tablemodel.DefaultAccountListModel;

public class FinCo {
	FincoFrame mainWindow;

	public FinCo(String configurationFile) {
		mainWindow = new FincoFrame();
		if (configurationFile == null) {
			setUpDefault();
		} else {
			setUp(configurationFile);
		}
	}

	public void addNewAccountButton(
			framework.view.buttons.PlugButton addNewAccountBtn) {
		mainWindow.addAddAccountButtons(addNewAccountBtn);
	}

	public void addTransactionButton(
			framework.view.buttons.PlugButton transactionBtn) {
		mainWindow.addTransactionButtons(transactionBtn);
	}

	public void setTitle(String title) {
		mainWindow.setTitle(title);
	}

	public void setAccountFactory(FincoFactory accountFactory) {
		AccountManager.INSTANCE.setAccountFactory(accountFactory);
	}

	public void setMailSender(String emailAddress) {
		MailConfiguration.INSTANCE.setEmailFrom(emailAddress);
	}

	public void setDataAccessFacade(DataAccessFacade dataAccessFacade) {
		AccountList.INSTANCE.setRepository(dataAccessFacade);
	}

	public void run() {
		try {
			try {
				UIManager.setLookAndFeel(UIManager
						.getSystemLookAndFeelClassName());
			} catch (Exception e) {
			}
			mainWindow.setVisible(true);
		} catch (Throwable t) {
			t.printStackTrace();
			System.exit(1);
		}
	}

	public static void main(String[] args) {
		FinCo app = new FinCo(null);
		app.run();
	}

	private void setUp(String configurationFile) {
		if (configurationFile == null) {
			setUpDefault();
			return;
		}
		Configuration con = new Configuration();
		try {
			con.DeserializeXML(configurationFile);
			setTitle(con.getApplicationTitle());
			setAccountFactory(XMLSerializeUtility
					.<FincoFactory> newInstance(con.getAbstractFactory()));
			setAccountListModel(con);
			addNewAccountButtons(con);
			addTransactionButtons(con);
			setDataAccessFacade(XMLSerializeUtility
					.<DataAccessFacade> newInstance(con.getDataAccessFacade()));
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			ex.printStackTrace();
		}
	}

	private void addTransactionButtons(Configuration con) throws Exception {
		if(con == null || con.getTransactionButton() == null || con.getTransactionButton().isEmpty()){
			addTransactionButton(new DepositButton());
			addTransactionButton(new WithdrawButton());
			addTransactionButton(new GenerateReport());
			return;
		}
		for (String btn : con.getTransactionButton()) {
			addTransactionButton(XMLSerializeUtility
					.<PlugButton> newInstance(btn));
		}
	}

	private void setAccountListModel(Configuration con) throws Exception {
		AccountListModel model;
		if(con == null || con.getTableList() == null || con.getTableList().isEmpty()){
			model = new DefaultAccountListModel();
		}else{
			model = XMLSerializeUtility
				.<AccountListModel> newInstance(con.getTableList());
		}
		mainWindow.setAccountListModel(model);
		AccountList.INSTANCE.addObserver(model);
	}

	private void addNewAccountButtons(Configuration con) throws Exception {
		if (con == null || con.getNewAccButton() == null
				|| con.getNewAccButton().isEmpty()) {
			addNewAccountButton(new framework.view.buttons.AddAccountBtn());
			return;
		}
		for (String btn : con.getNewAccButton()) {
			addNewAccountButton(XMLSerializeUtility
					.<PlugButton> newInstance(btn));
		}
	}

	private void setUpDefault() {
		try {
			setTitle("FinCo - Financial Framework (Group 3)");
			setAccountFactory(new DefaultFincoFactory());
			addNewAccountButtons(null);
			addTransactionButtons(null);
			setAccountListModel(null);
			setDataAccessFacade(new DefaultDataAccess());
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("Couldn't start" + e.getMessage());
		}
	}
}
