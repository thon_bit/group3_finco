package framework.view.form;

import java.awt.event.ActionEvent;

import framework.command.CreateAccount;
import framework.controller.AccountManager;

public abstract class AddAccountForm extends javax.swing.JDialog implements java.awt.event.ActionListener{
	
	private static final long serialVersionUID = 1L;
	
	public AddAccountForm(){
		
		JButton_OK.addActionListener(this);
		JButton_Cancel.addActionListener(this);
	}
	
	public javax.swing.JButton JButton_OK = new javax.swing.JButton();
	public javax.swing.JButton JButton_Cancel = new javax.swing.JButton();
	
	
	@Override
	public final void actionPerformed(ActionEvent event) {
		Object object = event.getSource();
		if (object == JButton_OK){
			framework.command.Transaction trns  = new CreateAccount(this, AccountManager.INSTANCE.getAccountFactory());
			AccountManager.INSTANCE.addTransaction(trns);
			dispose();
		}
		else if (object == JButton_Cancel)
			dispose();
	}
	
	public String getAccountType(){
		return "";
	}
	
	public abstract String getAccountIdentifier();

	public abstract String getAccountName();

	public abstract String getStreet();
	
	public abstract String getState();
	
	public abstract String getCity();
	
	public abstract String getZip();

	public abstract String getEmail();

}
