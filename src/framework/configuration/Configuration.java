package framework.configuration;

import java.util.ArrayList;
import java.util.List;

import framework.utility.XMLSerializeUtility;

public final class Configuration {
	private String applicationTitle;
	private String abstractFactory;
	private String tableList;
	private List<String> newAccButton;
	private List<String> transactionButton;
	private String dataAccessFacade;

	public Configuration(){
		newAccButton = new ArrayList<String>();
		transactionButton = new ArrayList<String>();
	}
	
	public Configuration(String at, String af,String tl,List<String> newAcc,List<String> transac, String daFacade){
		this.applicationTitle = at;
		this.abstractFactory = af;
		this.tableList = tl;
		this.newAccButton = newAcc;
		this.transactionButton = transac;
		this.dataAccessFacade = daFacade;
	}

	public String getApplicationTitle() {
		return applicationTitle;
	}

	public void setApplicationTitle(String applicationTitle) {
		this.applicationTitle = applicationTitle;
	}
	
	public String getAbstractFactory() {
		return abstractFactory;
	}

	public void setAbstractFactory(String abstractFactory) {
		this.abstractFactory = abstractFactory;
	}

	public String getTableList() {
		return tableList;
	}

	public void setTableList(String tableList) {
		this.tableList = tableList;
	}

	public List<String> getNewAccButton() {
		return newAccButton;
	}

	public void setNewAccButton(List<String> newAccButton) {
		this.newAccButton = newAccButton;
	}

	public List<String> getTransactionButton() {
		return transactionButton;
	}

	public void setTransactionButton(List<String> transactionButton) {
		this.transactionButton = transactionButton;
	}
	
	public String getDataAccessFacade() {
		return dataAccessFacade;
	}

	public void setDataAccessFacade(String dataAccessFacade) {
		this.dataAccessFacade = dataAccessFacade;
	}
	
	public void SerializeXML(String filename)throws Exception{
		XMLSerializeUtility.SerializeXML(this, filename);
	}
	public void DeserializeXML(String filename) throws Exception{
		Configuration con = XMLSerializeUtility.DeSerializeXML(filename);
		this.applicationTitle = con.applicationTitle;
		this.abstractFactory = con.abstractFactory;
		this.tableList = con.tableList;
		this.newAccButton = con.newAccButton;
		this.transactionButton = con.transactionButton;
		this.dataAccessFacade = con.dataAccessFacade;
	}
}
