package framework.view.buttons;

import java.awt.event.ActionEvent;
import java.util.Vector;
import framework.view.FincoFrame;

public abstract class PlugButton extends javax.swing.JButton implements java.awt.event.ActionListener{
	
	private static final long serialVersionUID = 1L;
	private FincoFrame mainwindow;
	
	public PlugButton(String title){
		this.setText(title);
		this.addActionListener(this);
	}
	
	public final void setMainWindow(FincoFrame mainwindow){
		this.mainwindow = mainwindow;
	}
	
	@Override
	public abstract void actionPerformed(ActionEvent arg0);
	
	public Vector<Object> getSelectedRow(){
		if(mainwindow != null){
			Vector<Object> selectedRow = mainwindow.getSelectedRow();
			return selectedRow;
		}
		return null;
	}
		
}
