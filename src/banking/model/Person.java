package banking.model;

import framework.model.Customer;
import framework.model.IPerson;

public class Person extends Customer implements IPerson{
	private static final long serialVersionUID = 1L;
	private String birthdate;
	
	public Person(String name, String street, String city, String state,
			String email,String date,String zip) {
		super(name, street, city, state, email,zip);
		// TODO Auto-generated constructor stub
		this.birthdate = date;
	}
	
	public void setBirthDate(String date){
		this.birthdate = date;
	}

	@Override
	public String getBirthdate() {
		return birthdate;
	}
}
